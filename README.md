# helpme

#### 介绍
急救脚本  
1.dump堆栈信息  
2.打印最耗费cpu的线程

#### 软件架构
原理查看如下两篇文章：  
[JAVA内存泄露查询](https://blog.51cto.com/4436396/2350346)  
[CPU飚高查询](https://blog.51cto.com/4436396/2118787)

#### 使用说明

1.  git clone https://gitee.com/feelgood3001/helpme.git
2.  cd helpme
3.  jps 拿到进程id
3.  sh ./helpme.sh 进程id online
4.  输入机器密码，相关文件将会传输到服务器的/export/目录下