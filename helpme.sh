#!/bin/bash

USAGE() {
  echo "usage: $0 <pid> <profile>(online，test,sim)"
}

if [ $# -lt 2 ]; then
  USAGE
  exit -1
fi

PID="$1"
echo "$PID"
PROFILE="$2"
echo "$2"

BASEDIR=/tmp/snhelp
LOGDIR=${BASEDIR}/sndump
DATE=$(date "+%Y%m%d%H%M%S")

RUN_DUMP() {
  if [[ $PROFILE == 'online' ]]; then
    ./dump --liveheap $PID
  fi
  if [[ $PROFILE == 'sim' ]]; then
    ./dump --liveheap $PID
  fi
  if [[ $PROFILE == 'sim' ]]; then
    ./dump-sim --liveheap $PID
  fi
}

RUN_BUSY_THREAD() {
  if [[ $PROFILE == 'online' ]]; then
    ./busy-thread 3 3 -c 20 -p $PID -a $LOGDIR/busy_threads_${DATE}_${PID}.log
  fi
  if [[ $PROFILE == 'sim' ]]; then
    ./busy-thread 3 3 -c 20 -p $PID -a $LOGDIR/busy_threads_${DATE}_${PID}.log
  fi
  if [[ $PROFILE == 'dev' ]]; then
    ./busy-thread-dev 3 3 -c 20 -p $PID -a $LOGDIR/busy_threads_${DATE}_${PID}.log
  fi
}

RUN_DUMP
echo 'RUN_DUMP end'

RUN_BUSY_THREAD
echo 'RUN_BUSY_THREAD end'

echo '开始打包'
FILE_NAME=""
if [ -x "$(command -v zip)" ]; then
  COMPRESS_FILE=${BASEDIR}/sndump-${PID}-${DATE}.zip
  zip -j ${COMPRESS_FILE} ${LOGDIR}/*
  FILE_NAME=$COMPRESS_FILE
else
  COMPRESS_FILE=${BASEDIR}/sndump-${PID}-${DATE}.tar.gz
  (cd ${LOGDIR} && tar -zcvf ${COMPRESS_FILE} *)
  FILE_NAME=$COMPRESS_FILE
fi

echo '打包完成，开始传输.........'
echo '请输入[192.168.101.26]密码：'
scp $FILE_NAME root@192.168.101.26:/export/
